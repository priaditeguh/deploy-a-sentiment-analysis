# SageMaker Deployment Project

Create a sentiment analysis model using IMDB movie reviews dataset and LSTM as its model. 
Then deploy the model using AWS SageMaker and simple web visualization. 
